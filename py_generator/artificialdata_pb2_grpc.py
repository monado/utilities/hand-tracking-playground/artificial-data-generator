# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

import artificialdata_pb2 as artificialdata__pb2


class ArtificialDataNexusStub(object):
    """The string reversal service definition.
    """

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.askForSequence = channel.unary_unary(
                '/artificialdata.ArtificialDataNexus/askForSequence',
                request_serializer=artificialdata__pb2.sequenceRequest.SerializeToString,
                response_deserializer=artificialdata__pb2.sequenceReply.FromString,
                )
        self.goodbye = channel.unary_unary(
                '/artificialdata.ArtificialDataNexus/goodbye',
                request_serializer=artificialdata__pb2.sayonara.SerializeToString,
                response_deserializer=artificialdata__pb2.Empty.FromString,
                )


class ArtificialDataNexusServicer(object):
    """The string reversal service definition.
    """

    def askForSequence(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def goodbye(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_ArtificialDataNexusServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'askForSequence': grpc.unary_unary_rpc_method_handler(
                    servicer.askForSequence,
                    request_deserializer=artificialdata__pb2.sequenceRequest.FromString,
                    response_serializer=artificialdata__pb2.sequenceReply.SerializeToString,
            ),
            'goodbye': grpc.unary_unary_rpc_method_handler(
                    servicer.goodbye,
                    request_deserializer=artificialdata__pb2.sayonara.FromString,
                    response_serializer=artificialdata__pb2.Empty.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'artificialdata.ArtificialDataNexus', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class ArtificialDataNexus(object):
    """The string reversal service definition.
    """

    @staticmethod
    def askForSequence(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/artificialdata.ArtificialDataNexus/askForSequence',
            artificialdata__pb2.sequenceRequest.SerializeToString,
            artificialdata__pb2.sequenceReply.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def goodbye(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/artificialdata.ArtificialDataNexus/goodbye',
            artificialdata__pb2.sayonara.SerializeToString,
            artificialdata__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
