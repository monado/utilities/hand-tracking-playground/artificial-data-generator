0, 1, 2, 3 were recorded on August 4, 2022 by Moses.

left_smoothed.csv was recorded sometime before that and should be fine

liveX.csv was recorded on september 8 using live tracking. Due to my oopsie with the old big model using GNLL, this should be better than the previous data.

Note that Palm is the first pose in there. Wrist is the second one, and identity, as expected.
